@extends('layout.master')
@section('judul')
    Halaman Index
@endsection

@section('isi')
    <h1>Buat Akun baru</h1>
    <h2>Sign up Form</h2>

    <form action="/signup" method="POST">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="lname" id="lname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" id="male" value="Male">Male<br><br>
        <input type="radio" name="gender" id="female" value="Female">Female<br><br>
        <input type="radio" name="gender" id="other" value="Other">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality"><br><br>
            <option value="1">Indonesian</option>
            <option value="2">Singapore</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select>
        <label>Languange Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br><br>
        <input type="checkbox" name="language">English <br><br>
        <input type="checkbox" name="language">Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio"cols="30" rows="10"></textarea> <br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection