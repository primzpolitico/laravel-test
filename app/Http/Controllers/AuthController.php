<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('/register');
    }

    public function signup(Request $request)
    {
        $fname = $request->fname;
        $lname = $request->lname;

        return view('welcome2', ['fname' => $fname, 'lname' => $lname]);
    }
}
